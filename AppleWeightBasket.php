<?php

class AppleWeightBasket
{
    const BASKET_COUNT = 7;
    public $capacity;

    public function calculateTotalAppleWeight($capacityPerBasket): float|int
    {
        return self::BASKET_COUNT * $capacityPerBasket;
    }
}