<?php
function moveZerosToEnd($input): string
{
    $numbers = explode(' ', $input);
    $nonZeros = array_filter($numbers, function ($num) {
        return $num !== '0';
    });
    $zeros = array_diff($numbers, $nonZeros);
    return implode(' ', array_merge($nonZeros, $zeros));
}

function chooseWordForm($number, $form1, $form2, $form5): string
{
    if ($number % 10 == 1 && $number % 100 !== 11) {
        return $number . ' ' . $form1;
    } elseif ($number % 10 >= 2 && $number % 10 <= 4 && ($number % 100 < 10 || $number % 100 >= 20)) {
        return $number . ' ' . $form2;
    } else {
        return $number . ' ' . $form5;
    }
}

function largestNumber($input): string
{
    $numbers = explode(' ', $input);
    usort($numbers, function ($a, $b) {
        return strcmp($b . $a, $a . $b);
    });
    return ltrim(implode('', $numbers), '0') ?: '0';
}